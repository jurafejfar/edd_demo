create docker image

using packer

```bash
packer build -only=docker debian-pg.json
```

or using docker

```bash
docker build -t registry.gitlab.com/jurafejfar/sandbox .
docker push registry.gitlab.com/jurafejfar/sandbox
```
run image
```bash
docker images
docker run -i -t -v `pwd`:/home/jura/work_dir --name mypgcontainer registry.gitlab.com/jurafejfar/sandbox /bin/bash
docker container ls -a
docker container rm -v mypgcontainer
```

run CI/CD jobs
```bash
gitlab-runner exec docker build1
gitlab-runner exec docker --docker-volumes `pwd`/build-output:/outdir build1
docker rm `docker ps -aq`
docker system prune -a
```

