# build and install PostgreSQL extension

See https://www.postgresql.org/docs/9.6/static/extend-extensions.html for details.

## within running container

* install extension with

```bash
sudo make install
```
* test extension

```bash
sudo -u vagrant make installcheck
```

## without container

* install extension with

```bash
sudo make install
```

* create DB superuser with the same name as is your Linux user (use Peer authentication)

```bash
make installcheck
```

* use environment variables to login (use password authentication)

```bash
PGUSER=yourDBsuperuser PGHOST=localhost make installcheck
```