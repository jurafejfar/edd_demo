
-- <function function_name="fn_generate_points" function_schema="sandbox" src="functions/fn_generate_points.sql">
CREATE OR REPLACE FUNCTION
sandbox.fn_generate_points_in_bounding_box
(
	_geom			geometry(Polygon,3035),
	_total			integer
)

RETURNS setof geometry(Point,3035)
AS
$$
DECLARE
gid		integer;
x		double precision;
y		double precision;
x_min		double precision;
x_max		double precision;
y_min		double precision;
y_max		double precision;
_grid_size	integer;
geom		geometry(POINT,3035);

BEGIN
	IF _geom IS NULL OR ST_IsEmpty(_geom)
	THEN
		RAISE EXCEPTION 'The geometry of stratum is NULL or empty!';
	END IF;

	IF _total IS NULL
	THEN
		RAISE EXCEPTION 'The total parameter has to be inserted!';
	END IF;

	gid = 1;
	x_min := (SELECT ST_XMin(ST_Envelope(_geom))::double precision);
	x_max := (SELECT ST_XMax(ST_Envelope(_geom))::double precision);
	y_min := (SELECT ST_YMin(ST_Envelope(_geom))::double precision);
	y_max := (SELECT ST_YMax(ST_Envelope(_geom))::double precision);
		
		WHILE gid <= _total LOOP
			gid = gid+1;
			x := (SELECT ((x_max - x_min) * random() + x_min)::double precision);
			y := (SELECT ((y_max - y_min) * random() + y_min)::double precision);
			
			RETURN NEXT ST_SetSRID(ST_MakePoint(x, y),3035) AS geom;
		END LOOP;

END;
$$
LANGUAGE plpgsql
STABLE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION sandbox.fn_generate_points_in_bounding_box(geometry(Polygon,3035),integer) IS
'Function generates points within bounding box of given polygon geometry.';

--select * from sandbox.fn_generate_points_in_bounding_box(st_setsrid(ST_GeomFromText('POLYGON((0 0,0 1,1 1,1 0,0 0))'),3035),30);
-- </function>