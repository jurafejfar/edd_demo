CREATE TABLE sandbox.t_points
(
  gid integer NOT NULL DEFAULT nextval(('sandbox.seq__t_points__gid'::text)::regclass),		-- automatic id used as a primary key
  geom geometry(Point,3035) NOT NULL, 								-- point geometry with SRID 3035
 
  CONSTRAINT pkey__t_points__gid PRIMARY KEY (gid),
  CONSTRAINT enforce_dims_the_geom CHECK (st_ndims(geom) = 2),
  CONSTRAINT enforce_geotype_geom CHECK (geometrytype(geom) = 'POINT'::text),
  CONSTRAINT enforce_srid_the_geom CHECK (st_srid(geom) = 3035)
);

COMMENT ON TABLE sandbox.t_points IS 'Table for random points.';
COMMENT ON COLUMN sandbox.t_points.gid IS 'Automatically generated ID as a primary key.';
COMMENT ON COLUMN sandbox.t_points.geom IS 'Point geometry with SRID 5221';

CREATE INDEX spidx__t_points__geom
  ON sandbox.t_points
  USING gist
  (geom);