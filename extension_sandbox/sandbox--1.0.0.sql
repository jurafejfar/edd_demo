---------------------------
-- sandbox
---------------------------
CREATE SCHEMA sandbox;

---------------------------
-- sandbox.c_country_code
---------------------------

CREATE TABLE sandbox.c_country_code(
id		integer,
label		character varying(100),
description	text,
CONSTRAINT pkey__c_country_code PRIMARY KEY (id)
);

COMMENT ON TABLE sandbox.c_country_code IS 'Looup table of country codes.';
COMMENT ON COLUMN sandbox.c_country_code.id IS 'Primary key, identifier of the country.';
COMMENT ON COLUMN sandbox.c_country_code.label IS 'Label (code) of the country.';
COMMENT ON COLUMN sandbox.c_country_code.description IS 'Description (name) of the country.';

-- data
INSERT INTO sandbox.c_country_code (id, label, description) VALUES (100, 'AL', 'SHQIPËRIA');
INSERT INTO sandbox.c_country_code (id, label, description) VALUES (200, 'AT', 'ÖSTERREICH');
INSERT INTO sandbox.c_country_code (id, label, description) VALUES (300, 'BE', 'BELGIQUE-BELGIË ');
INSERT INTO sandbox.c_country_code (id, label, description) VALUES (400, 'BG', 'БЪЛГАРИЯ (BULGARIA)');
INSERT INTO sandbox.c_country_code (id, label, description) VALUES (500, 'CH', 'SCHWEIZ/SUISSE/SVIZZERA');
INSERT INTO sandbox.c_country_code (id, label, description) VALUES (600, 'CY', 'ΚΥΠΡΟΣ (KYPROS)');
INSERT INTO sandbox.c_country_code (id, label, description) VALUES (700, 'CZ', 'ČESKÁ REPUBLIKA');
INSERT INTO sandbox.c_country_code (id, label, description) VALUES (800, 'DE', 'DEUTSCHLAND ');
INSERT INTO sandbox.c_country_code (id, label, description) VALUES (900, 'DK', 'DANMARK');
INSERT INTO sandbox.c_country_code (id, label, description) VALUES (1000, 'EE', 'EESTI');
INSERT INTO sandbox.c_country_code (id, label, description) VALUES (1100, 'EL', 'ΕΛΛΑΔΑ (ELLADA)');
INSERT INTO sandbox.c_country_code (id, label, description) VALUES (1200, 'ES', 'ESPAÑA ');
INSERT INTO sandbox.c_country_code (id, label, description) VALUES (1300, 'FI', 'SUOMI / FINLAND');
INSERT INTO sandbox.c_country_code (id, label, description) VALUES (1400, 'FR', 'FRANCE');
INSERT INTO sandbox.c_country_code (id, label, description) VALUES (1500, 'HR', 'HRVATSKA');
INSERT INTO sandbox.c_country_code (id, label, description) VALUES (1600, 'HU', 'MAGYARORSZÁG');
INSERT INTO sandbox.c_country_code (id, label, description) VALUES (1700, 'IE', 'IRELAND');
INSERT INTO sandbox.c_country_code (id, label, description) VALUES (1800, 'IS', 'ÍSLAND');
INSERT INTO sandbox.c_country_code (id, label, description) VALUES (1900, 'IT', 'ITALIA ');
INSERT INTO sandbox.c_country_code (id, label, description) VALUES (2000, 'LI', 'LIECHTENSTEIN');
INSERT INTO sandbox.c_country_code (id, label, description) VALUES (2100, 'LT', 'LIETUVA');
INSERT INTO sandbox.c_country_code (id, label, description) VALUES (2200, 'LU', 'LUXEMBOURG');
INSERT INTO sandbox.c_country_code (id, label, description) VALUES (2300, 'LV', 'LATVIJA');
INSERT INTO sandbox.c_country_code (id, label, description) VALUES (2400, 'ME', 'ЦРНА ГОРА (CRNA GORA)');
INSERT INTO sandbox.c_country_code (id, label, description) VALUES (2500, 'MK', 'ПОРАНЕШНА ЈУГОСЛОВЕНСКА РЕПУБЛИКА МАКЕДОНИЈА (PORANEŠNA JUGOSLOVENSKA REPUBLIKA MAKEDONIJA)');
INSERT INTO sandbox.c_country_code (id, label, description) VALUES (2600, 'MT', 'MALTA');
INSERT INTO sandbox.c_country_code (id, label, description) VALUES (2700, 'NL', 'NEDERLAND ');
INSERT INTO sandbox.c_country_code (id, label, description) VALUES (2800, 'NO', 'NORGE');
INSERT INTO sandbox.c_country_code (id, label, description) VALUES (2900, 'PL', 'POLSKA');
INSERT INTO sandbox.c_country_code (id, label, description) VALUES (3000, 'PT', 'PORTUGAL');
INSERT INTO sandbox.c_country_code (id, label, description) VALUES (3100, 'RO', 'ROMÂNIA');
INSERT INTO sandbox.c_country_code (id, label, description) VALUES (3200, 'RS', 'REPUBLIKA SRBIJA /РЕПУБЛИКА СРБИЈА');
INSERT INTO sandbox.c_country_code (id, label, description) VALUES (3300, 'SE', 'SVERIGE');
INSERT INTO sandbox.c_country_code (id, label, description) VALUES (3400, 'SI', 'SLOVENIJA');
INSERT INTO sandbox.c_country_code (id, label, description) VALUES (3500, 'SK', 'SLOVENSKO');
INSERT INTO sandbox.c_country_code (id, label, description) VALUES (3600, 'TR', 'TÜRKİYE');
INSERT INTO sandbox.c_country_code (id, label, description) VALUES (3700, 'UK', 'UNITED KINGDOM');


---------------------------
-- sandbox.f_a_country
---------------------------
CREATE TABLE sandbox.f_a_country(
gid			serial,
country_code		integer,
geom			geometry(MultiPolygon,3035),
CONSTRAINT pkey__f_a_country PRIMARY KEY (gid)
);

COMMENT ON TABLE sandbox.f_a_country IS 'Table of areas, where the estimation is done (cells), contains geometries.';
COMMENT ON COLUMN sandbox.f_a_country.country_code IS 'Foreign key, identifier of country code.';
COMMENT ON COLUMN sandbox.f_a_country.geom IS 'Polygon geometry of the cell.';

-- foreign key
ALTER TABLE sandbox.f_a_country
	ADD CONSTRAINT fkey__f_a_country__c_country_code FOREIGN KEY (country_code)
	REFERENCES sandbox.c_country_code (id)
	ON DELETE NO ACTION ON UPDATE CASCADE;

-- index
CREATE INDEX f_a_country_geom_idx
  ON sandbox.f_a_country
  USING gist
  (geom);

-- mark for dynamic data
SELECT pg_catalog.pg_extension_config_dump('sandbox.f_a_country', '');
SELECT pg_catalog.pg_extension_config_dump('sandbox.f_a_country_gid_seq', '');